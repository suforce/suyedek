import Ftp from '../models/ftp';
import Binary from '../models/binary';
import Compress from '../models/compress';
import Kullanici from '../models/kullanici';
import DizinYedekleme from '../models/dizinyedekleme';
import VeritabaniYedekleme from '../models/veritabaniyedekleme';

Ftp.sync();
Binary.sync();
Compress.sync();
Kullanici.sync();
DizinYedekleme.sync();
VeritabaniYedekleme.sync();