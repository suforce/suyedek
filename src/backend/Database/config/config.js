const os = require('os');
const path = require('path');
const highlight = require('cli-highlight').highlight;

require('dotenv').config();

module.exports = {
	dialect: 'sqlite',
	storage:
		(os.platform() != 'linux'
			? path.join(os.tmpdir(), '..', process.env.APP_NAME)
			: path.join(os.tmpdir(), process.env.APP_NAME)) + '/db.db',
	
};
