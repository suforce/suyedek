import { DataTypes, Model } from 'sequelize';
import { sequelize } from '.';

export default class Compress extends Model {}

Compress.init(
	{
		id: {
			type: DataTypes.UUID,
			defaultValue: DataTypes.UUIDV4,
			primaryKey: true,
		},
		kaynak: DataTypes.STRING({ length: 255 }),
		hedef1: DataTypes.STRING({ length: 255 }),
		hedef2: DataTypes.STRING({ length: 255 }),
		format: DataTypes.ENUM('rar', 'zip', 'tar'),
	},
	{
		sequelize,
		tableName: 'compress',
		timestamps: false,
		paranoid: false,
	},
);
