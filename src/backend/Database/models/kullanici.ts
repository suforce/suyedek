import { DataTypes, Model } from 'sequelize';
import { sequelize } from '.';
export default class Kullanici extends Model {}
Kullanici.init(
	{
		id: {
			type: DataTypes.UUID,
			primaryKey: true,
			defaultValue: DataTypes.UUIDV4,
		},
		kullaniciAdi: DataTypes.STRING(100),
		email: DataTypes.STRING(100),
		sifre: DataTypes.STRING(100),
	},
	{
		tableName: 'kullanici',
		sequelize,
		timestamps: false,
		paranoid: false,
	},
);
