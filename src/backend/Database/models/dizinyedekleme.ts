import Ftp from './ftp';
import Binary from './binary';
import { sequelize } from '.';
import Compress from './compress';
import { DataTypes, Model } from 'sequelize';

export default class Dizinyedekleme extends Model {}

Dizinyedekleme.init(
	{
		id: {
			type: DataTypes.UUID,
			defaultValue: DataTypes.UUIDV4,
			primaryKey: true,
		},
		yedeklemeAdi: DataTypes.STRING(255),
		yedekAciklama: DataTypes.TEXT,
		yedeklemeSaati: DataTypes.TIME,
		maxYedek: DataTypes.INTEGER,
		type: {
			type: DataTypes.ENUM,
			validate: {
				isIn: [veritabaniEnumToArray()],
			},
		},
		kaynak: DataTypes.STRING({ length: 255 }),
		hedef1: DataTypes.STRING({ length: 255 }),
		hedef2: DataTypes.STRING({ length: 255 }),
	},
	{
		sequelize,
		tableName: 'dizinyedekleme',
		timestamps: false,
		paranoid: false,
		// indexes: [{ fields: ['ftp', 'binary', 'compress'] }],
	},
);
Dizinyedekleme.belongsTo(Binary, { as: 'binary' });
Dizinyedekleme.belongsTo(Compress, { as: 'compress' });
Dizinyedekleme.belongsTo(Ftp, { as: 'ftp' });
