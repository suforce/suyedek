import { Sequelize } from 'sequelize';
import constants from '../../../constants';

export const sequelize = new Sequelize({
	dialect: 'sqlite',
	storage: constants.TEMP_PATH + '/db.db',
});
