import Ftp from './ftp';
import Binary from './binary';
import { sequelize } from '.';
import Compress from './compress';
import { DataTypes, Model } from 'sequelize';

export default class VeritabaniYedekleme extends Model {}

VeritabaniYedekleme.init(
	{
		id: {
			type: DataTypes.UUID,
			defaultValue: DataTypes.UUIDV4,
			primaryKey: true,
		},
		yedeklemeAdi: DataTypes.STRING(255),
		yedekAciklama: DataTypes.TEXT,
		sunucuAdresi: DataTypes.STRING({ length: 255 }),
		sunucuPort: DataTypes.INTEGER({ length: 5 }),
		sunucuKullaniciAdi: DataTypes.STRING({ length: 255 }),
		sunucuSifre: DataTypes.STRING({ length: 255 }),
		veritabaniAdi: DataTypes.STRING({ length: 255 }),
		yedeklemeGun: {
			type: DataTypes.ENUM,
			validate: {
				isIn: [periyotlarToArray()],
			},
		},
		yedeklemeSaati: DataTypes.TIME,
		maxYedek: DataTypes.INTEGER,
		type: {
			type: DataTypes.ENUM,
			validate: {
				isIn: [veritabaniEnumToArray()],
			},
		},
		hedef1: DataTypes.STRING({ length: 255 }),
		hedef2: DataTypes.STRING({ length: 255 }),
	},
	{
		sequelize,
		tableName: 'veritabaniyedekleme',
		timestamps: false,
		paranoid: false,
		// indexes: [{ fields: ['ftp', 'binary', 'compress'] }],
	},
);
VeritabaniYedekleme.hasOne(Binary, { as: 'binary' });
VeritabaniYedekleme.hasOne(Compress, { as: 'compress' });
VeritabaniYedekleme.hasOne(Ftp, { as: 'ftp' });
