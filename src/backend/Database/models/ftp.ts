import { DataTypes, Model } from 'sequelize';
import { sequelize } from '.';

export default class Ftp extends Model {}
Ftp.init(
	{
		id: {
			type: DataTypes.UUID,
			primaryKey: true,
			defaultValue: DataTypes.UUIDV4,
		},
		sunucuAdresi: DataTypes.STRING({ length: 255 }),
		sunucuPort: DataTypes.INTEGER({ length: 3 }),
		sunucuKullaniciAdi: DataTypes.STRING({ length: 255 }),
		sunucuSifre: DataTypes.STRING({ length: 255 }),
		hedef: DataTypes.STRING(255),
	},
	{
		sequelize,
		tableName: 'ftp',
		timestamps: false,
		paranoid: false,
	},
);
