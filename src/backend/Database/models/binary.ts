import { DataTypes, Model } from 'sequelize';
import { sequelize } from '.';

export default class Binary extends Model {}
Binary.init(
	{
		id: {
			type: DataTypes.UUID,
			primaryKey: true,
			defaultValue: DataTypes.UUIDV4,
		},
		kaynak: DataTypes.STRING({ length: 255 }),
		hedef1: DataTypes.STRING({ length: 255 }),
		hedef2: DataTypes.STRING({ length: 255 }),
	},
	{
		sequelize,
		tableName: 'binary',
		timestamps: false,
		paranoid: false,
	},
);
