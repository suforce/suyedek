import fse from 'fs-extra';

export default abstract class ICron {
	yedekYolu: string;
	yedekler: Array<any>;
	islem: 'mysql' | 'mssql' | 'dizin';

	yedekleriGetir() {
		return fse.readJSON(this.yedekYolu);
	}
}
