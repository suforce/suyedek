import path from 'path';
import fse from 'fs-extra';
import schedule from 'node-schedule';
import constants from '../../constants';

export default class Cron {
	veritabanlariniKontrolEt() {
		const mysqlYedekYolu = path.join(constants.TEMP_PATH, 'mysql');
		if (!fse.existsSync(mysqlYedekYolu)) fse.mkdirsSync(mysqlYedekYolu);
        
	}
}
