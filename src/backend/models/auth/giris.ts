import Kullanici from '../../Database/models/kullanici';

export default class Giris {
	kullaniciAdi?: string;
	sifre: string;
	email: string;

	constructor(params: { sifre: string; email?: string; kullaniciAdi?: string }) {
		if (!params.kullaniciAdi && !params.email) throw new Error('Kullanıcı Adı Yada Emailden Birisi Olmalıdır !');
		this.sifre = params.sifre;
		this.email = params.email;
		this.kullaniciAdi = params.kullaniciAdi;
	}

	async girisYap() {
		console.log(await Kullanici.findAndCountAll());
	}
}
