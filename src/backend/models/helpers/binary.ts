import path from 'path';
import backup from 'backup';
import moment from 'moment';
import IYardimci from '../Interfaces/IYardimci';

export default class BinaryBackup extends IYardimci {
	override islemYap(): boolean | void {
		this.yedekle(this.hedef1);
		if (this.hedef2) this.yedekle(this.hedef2);
	}

	private yedekle(yol: string) {
		const hedef = path.join(
			yol,
			`${path.basename(this.kaynak).replace(/\.sql/, '')}_${moment().format('DDMMYYYYHHmmss')}.backup`,
		);
		backup.backup(this.kaynak, hedef);
	}
}
