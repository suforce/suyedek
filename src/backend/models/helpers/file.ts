import fs from 'fs';
import constants from '../../../constants';

export default function hazirliklariYap() {
	if (!fs.existsSync(constants.TEMP_PATH)) fs.mkdirSync(constants.TEMP_PATH);
}
