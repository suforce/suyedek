import fse from 'fs-extra';
import path from 'path';
import compressing from 'compressing';
import IYardimci from '../Interfaces/IYardimci';

export default class Compressor extends IYardimci {
	format: 'rar' | 'zip';

	constructor(cons: { kaynak: string; hedef1: string; hedef2?: string; format: 'rar' | 'zip' }) {
		super(cons);
		this.format = cons.format;
	}

	async islemYap() {
		let hedef = path.join(this.hedef1, path.basename(this.kaynak));
		let hedef2 = this.hedef2 ? path.join(this.hedef2, path.basename(this.kaynak)) : null;
		if (!fse.existsSync(this.hedef1)) fse.mkdirpSync(this.hedef1);
		if (this.hedef2 && !fse.existsSync(this.hedef2)) fse.mkdirpSync(this.hedef2);
		if (!hedef.endsWith(this.format)) hedef += '.' + this.format;
		if (hedef2 && !hedef2.endsWith(this.format)) hedef2 = '.' + this.format;
		switch (this.format) {
			case 'rar':
				if (fse.lstatSync(this.kaynak).isDirectory()) await compressing.tar.compressDir(this.kaynak, hedef);
				else await compressing.tar.compressFile(this.kaynak, hedef);
				if (this.hedef2) {
					if (fse.lstatSync(this.kaynak).isDirectory())
						await compressing.tar.compressDir(this.kaynak, hedef2);
					else await compressing.tar.compressFile(this.kaynak, hedef2);
				}
				break;
			case 'zip':
				if (fse.lstatSync(this.kaynak).isDirectory()) await compressing.zip.compressDir(this.kaynak, hedef);
				if (this.hedef2) {
					if (fse.lstatSync(this.kaynak).isDirectory())
						await compressing.zip.compressDir(this.kaynak, hedef2);
					else await compressing.zip.compressFile(this.kaynak, hedef2);
				}
				break;
		}
	}
}
