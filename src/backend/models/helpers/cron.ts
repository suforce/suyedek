import schedule, { JobCallback } from 'node-schedule';
import ICron from '../../cron/ICron';

export default class CronIslemleri extends ICron {
	SetCronJob(islem: JobCallback, rule: string) {
		schedule.scheduleJob(rule, islem);
	}
}

/*

    Minute   Hour   Day of Month       Month          Day of Week
    (0-59)  (0-23)     (1-31)    (1-12 or Jan-Dec)  (0-6 or Sun-Sat)
      0        2          12             *                *         
   
*/