import fs from 'fs';
import path from 'path';
import Client from 'ftp';
import dotenv from 'dotenv';
import Compressor from './compressor';
import constants from '../../../constants';
import IYardimci from '../Interfaces/IYardimci';

dotenv.config();
export default class FtpUpload extends IYardimci {
	sunucuAdres: string;
	sunucuPort = 21;
	sunucuKullaniciAdi: string;
	sunucuSifre: string;
	yuklenecekDizin: string;

	constructor(params: {
		adres: string;
		kullaniciAdi: string;
		sifre: string;
		kaynak: string;
		hedef1: string;
		hedef2?: string;
		port?: 21;
	}) {
		super(params);
		this.sunucuAdres = params.adres;
		this.sunucuPort = params.port || 21;
		this.sunucuKullaniciAdi = params.kullaniciAdi;
		this.sunucuSifre = params.sifre;
		this.kaynak = params.kaynak;
		this.yuklenecekDizin = params.hedef1;
	}

	override islemYap(): boolean | void | Promise<void> | Promise<boolean> {
		try {
			const client = new Client();
			const zipHedefi = path.join(constants.TEMP_PATH, path.basename(this.kaynak) + '.rar');
			const hedef = path.join(this.yuklenecekDizin, path.basename(this.kaynak) + '.rar');
			new Compressor({
				kaynak: this.kaynak,
				format: 'rar',
				hedef1: constants.TEMP_PATH,
			}).islemYap();
			client.on('ready', function () {
				client.put(zipHedefi, hedef, function (err) {
					if (err) throw err;
					client.end();
				});
			});
			client.on('close', () => fs.rmSync(zipHedefi));
			client.connect({
				host: this.sunucuAdres,
				port: this.sunucuPort,
				user: this.sunucuKullaniciAdi,
				password: this.sunucuSifre,
			});
			return true;
		} catch {
			return false;
		}
	}
}
