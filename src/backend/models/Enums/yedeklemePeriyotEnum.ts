enum Periyotlar {
	'pazartesi',
	'sali',
	'carsamba',
	'persembe',
	'cuma',
	'cumartesi',
	'pazar',
}

function periyotlarToArray(): Array<string> {
	const dizi = Object.values(Periyotlar).map((x) => dizi[x]);
	return dizi;
}
