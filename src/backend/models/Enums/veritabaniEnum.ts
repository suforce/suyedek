enum veritabaniEnum {
	'mysql',
	'mssql',
}

function veritabaniEnumToArray(): Array<string> {
	const dizi = Object.values(veritabaniEnum).map((x) => dizi[x]);
	return dizi;
}
