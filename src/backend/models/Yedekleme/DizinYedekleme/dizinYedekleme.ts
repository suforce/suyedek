import path from 'path';
import fse from 'fs-extra';
import IYardimci from '../../Interfaces/IYardimci';
import IYedekleme from '../../Interfaces/IYedekleme';

export default class DizinYedekleme implements IYedekleme {
	kaynak: string;
	hedef1: string;
	hedef2?: string;
	YardimciIslemler?: Array<IYardimci>;
	yedeklemePeriyotu: Periyotlar;
	yedeklemeSaati: string;
	maxYedek?: number;

	constructor(cons: { kaynak: string; hedef1: string; hedef2?: string; yardimcilar?: Array<IYardimci> }) {
		this.kaynak = cons.kaynak;
		this.hedef1 = cons.hedef1;
		this.hedef2 = cons.hedef2;
		this.YardimciIslemler = cons.yardimcilar;
	}

	async yedekle() {
		const hedef1 = this.dizinleriOlustur(this.kaynak, this.hedef1);
		fse.copySync(this.kaynak, hedef1, { overwrite: true, recursive: true });
		if (this.hedef2) {
			const hedef2 = this.dizinleriOlustur(this.kaynak, this.hedef2);
			fse.copySync(this.kaynak, hedef2, { overwrite: true, recursive: true });
		}
		if (this.YardimciIslemler) {
			for (const yardimci of this.YardimciIslemler) {
				await yardimci.islemYap();
			}
		}
	}

	private dizinleriOlustur(kaynak: string, hedef: string) {
		const newPath = path.join(hedef, path.basename(kaynak));
		fse.mkdirpSync(newPath);
		return newPath;
	}
}
