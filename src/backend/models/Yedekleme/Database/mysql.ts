import path from 'path';
import fse from 'fs-extra';
import mysql from 'mysql2';
import mysqldump from 'mysqldump';
import IDatabase from '../../Interfaces/IDatabase';
import VeritabaniYedekleme from '../../../Database/models/veritabaniyedekleme';

export default class MysqlYedekleme extends IDatabase {
	connect() {
		const connection = mysql.createConnection({
			host: this.databaseHost,
			port: this.databasePort,
			user: this.databaseUserName,
			password: this.databasePassword,
			database: this.databaseName,
		});
		let sonuc: boolean;
		connection.connect(function (error) {
			if (error) sonuc = false;
			else sonuc = true;
		});
		return sonuc;
	}

	override async yedekle() {
		if (!fse.existsSync(this.hedef1)) fse.mkdirpSync(this.hedef1);
		const hedef = await this.islemYap(this.hedef1);
		if (this.hedef2) {
			if (!fse.existsSync(this.hedef2)) fse.mkdirpSync(this.hedef2);
			this.islemYap(this.hedef2);
		}
		if (this.YardimciIslemler) {
			for (const yardimci of this.YardimciIslemler) {
				yardimci.kaynak = hedef;
				await yardimci.islemYap();
			}
		}
	}

	private async islemYap(hedef: string) {
		hedef = path.join(hedef, this.databaseName + '.sql');
		await mysqldump({
			connection: {
				host: this.databaseHost,
				port: this.databasePort,
				user: this.databaseUserName,
				password: this.databasePassword,
				database: this.databaseName,
			},
			dumpToFile: hedef,
		});
		return hedef;
	}
}
