import mssql from 'mssql';
import IDatabase from '../../Interfaces/IDatabase';

export default class MssqlYedekleme extends IDatabase {
	override connect(): boolean {
		let sonuc: boolean;
		try {
			mssql.connect({
				server: this.databaseHost,
				port: this.databasePort,
				user: this.databaseUserName,
				password: this.databasePassword,
				database: this.databaseName,
				pool: {
					max: 10,
					min: 0,
					idleTimeoutMillis: 30000,
				},
			});
			sonuc = true;
		} catch {
			sonuc = false;
		}
		return sonuc;
	}
	yedekle(): void {
		if (this.YardimciIslemler) {
			for (const yardimci of this.YardimciIslemler) {
				yardimci.islemYap();
			}
		}
	}
}
