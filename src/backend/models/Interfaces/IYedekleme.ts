export default interface IYedekleme {
	yedeklemePeriyotu: Periyotlar;
	yedeklemeSaati: string;
	maxYedek?: number;
}
