export default abstract class IYardimci {
	kaynak: string;
	hedef1: string;
	hedef2?: string;
	
	constructor(cons: { kaynak: string; hedef1: string; hedef2?: string }) {
		this.kaynak = cons.kaynak;
		this.hedef1 = cons.hedef1;
		this.hedef2 = cons.hedef2;
	}
	abstract islemYap(): boolean | void | Promise<void> | Promise<boolean>;
}
