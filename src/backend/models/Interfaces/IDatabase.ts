import IYardimci from './IYardimci';
import IYedekleme from './IYedekleme';

export default abstract class IDatabase implements IYedekleme {
	databaseHost: string;
	databasePort: number;
	databaseUserName: string;
	databasePassword: string;
	databaseName: string;
	hedef1: string;
	yedeklemePeriyotu: Periyotlar;
	yedeklemeSaati: string;
	maxYedek?: number;
	hedef2?: string;
	YardimciIslemler?: Array<IYardimci>;

	constructor(params: {
		port: number;
		user: string;
		password: string;
		database: string;
		hedef1: string;
		yedeklemePeriyotu: Periyotlar;
		yedeklemeSaati: string;
		maxYedek?: number;
		host?: string;
		hedef2?: string;
		yardimciIslemler?: Array<IYardimci>;
	}) {
		this.databaseHost = params.host || 'localhost';
		this.databasePort = params.port;
		this.databaseUserName = params.user;
		this.databasePassword = params.password;
		this.databaseName = params.database;
		this.hedef1 = params.hedef1;
		this.hedef2 = params.hedef2;
		this.yedeklemePeriyotu = params.yedeklemePeriyotu;
		this.yedeklemeSaati = params.yedeklemeSaati;
		this.maxYedek = params.maxYedek;
		this.YardimciIslemler = params.yardimciIslemler;
	}

	abstract connect(): boolean | Promise<boolean>;
	abstract yedekle(): void | Promise<void>;
}
