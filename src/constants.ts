import os from 'os';
import path from 'path';
import dotenv from 'dotenv';

dotenv.config();
export default {
	TEMP_PATH:
		os.platform() != 'linux'
			? path.join(os.tmpdir(), '..', process.env.APP_NAME)
			: path.join(os.tmpdir(), process.env.APP_NAME),
};
