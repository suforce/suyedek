import IDatabase from '../../src/backend/models/Interfaces/IDatabase';
import VeritabaniYedekleme from '../../src/backend/Database/models/veritabaniyedekleme';

export default class DatabaseApi {
	static async veritabaniYedeklemeCreate(model: IDatabase, type: 'mysql' | 'mssql') {
		VeritabaniYedekleme.create({
			sunucuAdresi: model.databaseHost,
			sunucuPort: model.databasePort,
			sunucuKullaniciAdi: model.databaseUserName,
			sunucuSifre: model.databasePassword,
			veritabaniAdi: model.databaseName,
		});
	}
}
